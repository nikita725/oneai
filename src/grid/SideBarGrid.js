import React from 'react';
import styled from 'styled-components';
import SideBar from '../components/SideBar';

const StyledSideBarGrid = styled.div`
    grid-area: sidebar;
`;

const SideBarGrid = () => {
    return (
        <StyledSideBarGrid>
            <SideBar/>
        </StyledSideBarGrid>
    );
};

export default SideBarGrid;