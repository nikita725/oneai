import React from 'react';
import styled from 'styled-components';

import Terminal from '../components/Terminal';
import AIBox from '../components/AIBox';

const StyledContentSectionGrid = styled.div.attrs({ className: 'contentsection-wrapper' })`
    grid-area: contentsection;
    padding: 64px;
    display: grid;
    grid-template-areas: 'terminal aibox';
    // grid-template-rows: 100%;
    grid-template-columns: 67.5% auto;
    grid-gap: 32px;
    overflow: auto;

    @media(max-width: 1392px) {
        padding-left: 32px;
        padding-right: 32px;
    }
    
    @media(max-width: 1024px) {
        grid-template-areas: 
            'terminal'
            'aibox';
        grid-template-rows: 1hr 1hr;
        grid-template-columns: 100%;
    }
`;

class ContentSectionGrid extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            wsMsg: [],
        };
    }

    setWsMsg = (data) => {
        this.state.wsMsg.push(data);
        this.forceUpdate();
    }

    resetWsMsg = () => {
        this.state.wsMsg = [];
        this.forceUpdate();
    }

    render() {
        const { wsMsg } = this.state;

        return (
            <StyledContentSectionGrid>
                <Terminal
                    wsMsg={wsMsg}
                    setWsMsg={this.setWsMsg}
                    resetWsMsg={this.resetWsMsg}
                />
                <AIBox
                    wsMsg={wsMsg}
                />
            </StyledContentSectionGrid>
        );
    }
}

export default ContentSectionGrid;
