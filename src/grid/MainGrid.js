import React from 'react';
import styled from 'styled-components';
import SideBarGrid from './SideBarGrid';
import ContentSectionGrid from './ContentSectionGrid';

const GridWrapper = styled.div`
    display: grid;
    grid-template-rows: 100%;
    grid-gap: 0;
    height: 100%;
    background: ${props => props.theme.palette.clrBackground};
    grid-template-areas: 'sidebar contentsection';
    grid-template-columns: minmax(256px, 17%) auto;
    
    @media(max-width: 1280px) {
        grid-template-areas: 'contentsection';
        grid-template-columns: 100%;
    }
`;

const MainGrid = () => (
    <GridWrapper>
        <SideBarGrid/>
        <ContentSectionGrid/>
    </GridWrapper>
);

export default MainGrid;
