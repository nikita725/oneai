/* eslint-disable */
import React from 'react';
import styled from 'styled-components';
import aiSvg from './assets/ai.svg';

const AIBoxWrapper = styled.div`
    grid-area: aibox;
    background: ${props => props.theme.palette.clrContrastText};
    border-radius: 8px;
`;

const Head = styled.div`
    padding: 27px 22px;
    display: flex;
    align-items: center;
    
    > span {
        padding-left: 22px;
        font-size: 16px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.5;
        letter-spacing: normal;
        text-align: left;
        color: ${props => props.theme.palette.clrText};
    }
`;

const Stats = styled.div`
    width: 100%;
    margin-left: auto;
    margin-right: auto;
    max-width: 300px;
    padding-left: 20px;
    padding-right: 20px;
    margin-top: 98px;
    margin-bottom: 98px;
    
    font-size: 16px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    text-align: left;
    color: ${props => props.theme.palette.clrText};
    
    .item {
        display: flex;
        align-items: center;
        padding-bottom: 12px;
        
        .shape {
            width: 20px;
            display: flex;
            justify-content: center;
        }
        
        .ellipse {
            width: 12px !important;
            height: 12px !important;
            &.red {
                background-color: ${props => props.theme.palette.clrHover};
            }
            &.green {
                background-color: ${props => props.theme.palette.clrGreen};
            }
            border-radius: 100%;
        }
        
        .rect {
            width: 20px;
            height: 1px;
            border-radius: 4px;
            &.green {
                border: solid 4px ${props => props.theme.palette.clrGreen};
                background-color: ${props => props.theme.palette.clrGreen};
            }           
            &.blue {
                border: solid 4px ${props => props.theme.palette.clrBlue};
                background-color: ${props => props.theme.palette.clrBlue};
            } 
        }
        
        .label {
            padding-left: 21px;
        }
    }
`;

const CanvasWrapper = styled.div`
    display: flex;
    justify-content: center;
`;

class AIBox extends React.Component {
    constructor(props) {
        super(props);
        this.canvasRef = React.createRef();
        this.tick = null;
        this.eventsList = [];
        this.nodesList = [];
    }

    componentWillReceiveProps(props) {
        for (let i = 0; i < props.wsMsg.length; i++) {
            const wsMsg = JSON.parse(props.wsMsg[i]);
            if (wsMsg && wsMsg.type === 'ok') {
                this.eventsList = [];
                this.nodesList = [];
                this.count = 0;
                clearInterval(this.tick);
                this.tick = setTimeout(this.renderCanvas, 2000);
            }
            if (wsMsg && wsMsg.type === 'event') {
                if (wsMsg.event && wsMsg.event.event === 'agent_create') {
                    this.count++;
                }
            }
            this.eventsList.push(wsMsg);
        }
    }

    getPointFromId = (id) => {
        for (let i = 0; i < this.nodesList.length; i++) {
            if (this.nodesList[i].id === id) {
                return this.nodesList[i];
            }
        }
    }

    renderCanvas = async () => {
        const canvas = this.canvasRef.current;
        const ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        const x = 150;
        const y = 25;
        const r = 125;
        const alpha = Math.PI * 2 / this.count;
        let circleIndex = 0;

        for (let i = 0; i < this.eventsList.length; i++) {
            const activeMsg = (this.eventsList[i] && this.eventsList[i].type === 'event') ? this.eventsList[i].event : null;

            if (activeMsg) {
                const msg = activeMsg.event ? activeMsg.event : activeMsg.type;
                switch (msg) {
                case 'agent_create':
                    let obj = {};
                    obj.x = x + r * Math.sin(alpha * circleIndex);
                    obj.y = y - (Math.cos(alpha * circleIndex) - 1) * r;
                    obj.id = activeMsg.id;
                    this.nodesList.push(obj);

                    let circle = new Path2D();
                    circle.arc(obj.x, obj.y, 10, 0, 2 * Math.PI);
                    ctx.fillStyle = '#41d98d';
                    ctx.fill(circle);

                    circleIndex++;
                    break;
                case 'link_create':
                    const fromId = activeMsg.from;
                    const toId = activeMsg.to;
                    const fromPt = this.getPointFromId(fromId);
                    const toPt = this.getPointFromId(toId);

                    ctx.strokeStyle = '#41d98d';
                    ctx.moveTo(fromPt.x, fromPt.y);
                    ctx.lineTo(toPt.x, toPt.y);
                    ctx.stroke();
                    break;
                case 'display_it':

                    break;
                case 'input_confirmation':

                    break;
                case 'agent_destroy':

                    break;

                case 'input':

                    break;
                case 'sending':

                    break;
                case 'calculation':

                    break;
                default:
                    break;
                }
                await this.waitFor();
            }
        }
    }

    waitFor = () => {
        return new Promise(resolve => {
            setTimeout(() => { resolve(); }, 500);
        });
    };

    render() {
        return (
            <AIBoxWrapper>
                <Head>
                    <img src={aiSvg} alt=""/>
                    <span>AI Display</span>
                </Head>
                <CanvasWrapper>
                    <canvas width="300" height="300" ref={this.canvasRef} />
                </CanvasWrapper>
                <Stats>
                    <div className="item">
                        <div className="shape">
                            <div className="ellipse green"/>
                        </div>
                        <span className="label">Agent Creation</span>
                    </div>
                    <div className="item">
                        <div className="shape">
                            <div className="ellipse red"/>
                        </div>
                        <span className="label">Inactive/terminated</span>
                    </div>
                    <div className="item">
                        <div className="shape">
                            <div className="rect green"/>
                        </div>
                        <span className="label">Link Created</span>
                    </div>
                    <div className="item">
                        <div className="shape">
                            <div className="rect blue"/>
                        </div>
                        <span className="label">Message passed</span>
                    </div>
                </Stats>
            </AIBoxWrapper>
        );
    }
}

export default AIBox;
