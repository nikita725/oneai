import React, { Component } from 'react';
import styled from 'styled-components';
// import logo from './assets/logo.svg';

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    cursor: pointer;
    margin-bottom: 46px;
    
    .item-svg {
        background: ${props => props.active ? "url('/img/sidebar/menu" + props.level + "_on.svg')" : ''};
    }
    
    .item-label {
        color: ${props => props.active ? props.theme.palette.clrHover : ''};
    }
    
    &:hover {
        .item-svg {
            background: ${props => "url('/img/sidebar/menu" + props.level + "_on.svg')"};
        }
        .item-label {
            color: ${props => props.theme.palette.clrHover};
        }
    }
`;

const ItemSvg = styled.div.attrs({ className: 'item-svg' })`
    background: ${props => "url('/img/sidebar/menu" + props.level + ".svg')"};
    background-size: 100% 100%;
    width: 20px;
    height: 20px;
`;

const Label = styled.div.attrs({ className: 'item-label' })`
    font-size: 16px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal;
    text-align: left;
    color: ${props => props.theme.palette.clrText};
    padding-left: 26px;
`;

class Item extends Component {
    render() {
        const {
            name,
            level,
            index,
            onClick,
        } = this.props;

        return (
            <Wrapper level={level} active={index === level} onClick={() => onClick(level)}>
                <ItemSvg level={level}/>
                <Label>{name}</Label>
            </Wrapper>
        );
    }
}

export default Item;
