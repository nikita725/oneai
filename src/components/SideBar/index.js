import React, { Component } from 'react';
import styled from 'styled-components';
import logo from './assets/logo.svg';
import Item from './Item';

const Wrapper = styled.div.attrs({ className: 'sidebar-wrapper' })`
    height: 100%;
    background: #fff;
    padding: 64px 26px;
`;

const LogoWrapper = styled.div`
    display: flex;
    justify-content: center;
    margin-bottom: 143px;
`;

const LogoLabel = styled.span`
    font-family: Arial;
    font-size: 19px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.53;
    letter-spacing: normal;
    text-align: left;
    color: ${props => props.theme.palette.clrText};
`;

class Sidebar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            index: 1,
        };
    }

    clickStep = (idx) => {
        this.setState({
            index: idx,
        });
    }

    render() {
        const { index } = this.state;

        return (
            <Wrapper>
                <LogoWrapper>
                    <img src={logo} alt=""/>
                    <LogoLabel>AI</LogoLabel>
                </LogoWrapper>

                <Item name="Level 1" level={1} index={index} onClick={this.clickStep}/>
                <Item name="Level 2" level={2} index={index} onClick={this.clickStep}/>
                <Item name="Level 3" level={3} index={index} onClick={this.clickStep}/>
                <Item name="Level 4" level={4} index={index} onClick={this.clickStep}/>
                <Item name="Level 5" level={5} index={index} onClick={this.clickStep}/>
                <Item name="Level 6" level={6} index={index} onClick={this.clickStep}/>
                <Item name="Level 7" level={7} index={index} onClick={this.clickStep}/>
            </Wrapper>
        );
    }
}

export default Sidebar;
