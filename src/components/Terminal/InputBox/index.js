import React from 'react';
import styled from 'styled-components';

import iconInput from '../assets/input.svg';
import gradient from '../assets/gradient.svg';

const InputWrapper = styled.div`
    width: 100%;
    background: ${props => props.theme.palette.clrContrastText};
    margin-bottom: 32px;
    border-radius: 8px;
    display: flex;
    flex-direction: column;
    overflow: hidden;
`;

const Head = styled.div`
    padding: 27px 22px;
    display: flex;
    align-items: center;
    
    > span {
        padding-left: 22px;
        font-size: 16px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.5;
        letter-spacing: normal;
        text-align: left;
        color: ${props => props.theme.palette.clrText};
    }
`;

const Body = styled.div`
    padding: 8px 63px;
`;

const Label = styled.div`
    display: flex;
    align-items: center;
    
    > span {
        flex: 1;
        font-size: 16px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.5;
        letter-spacing: normal;
        text-align: left;
        color: ${props => props.theme.palette.clrDarkText};
    }
    
    > button {
        margin-left: 20px;
        border-radius: 10px;
        outline: none;
        cursor: pointer;
        padding: 0 30px;
        height: 30px;
        
        &:hover {
            background: #00aeef;
        }
    }
`;

const Input = styled.textarea`
    width: calc(100% - 60px);
    height: 150px;
    border-radius: 32px;
    background-color: ${props => props.theme.palette.clrBackground};
    outline: none;
    border: 0;
    padding: 30px;
    font-size: 16px;
    margin-bottom: 23px;
    margin-top: 13px;
    resize: none;
`;

class InputBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            codes: '',
        };
        this.tickSend = null;
    }

    componentDidMount() {
        // const mockCodes = 'agent1 = Create(1 _, [], [ plus_one, plus_one ], [\n' +
        //     '   Create_DisplayIt(2 display 2)\n' +
        //     '] 1),\n' +
        //     '\n' +
        //     'Create_Input(1 input, [agent1] 1),\n' +
        //     '\n' +
        //     'message_send[input, 1]';
        const mockCodes = 'Make(1 A, 10, @Create[ _, [], [ transmit ], [] ] 1),\n' +
            'Make(1 B, 2, @Create[ _, [], [ transmit ], [] ] 1),\n' +
            'Bridge[ [ 3,4 ], [ B ], [ A ] ]';

        this.setState({
            codes: mockCodes,
        });
    }

    changeCodes = (e) => {
        this.setState({
            codes: e.target.value,
        });
    };

    sendData = () => {
        this.props.sendWSData(this.state.codes || '');
        clearInterval(this.tickSend);
        this.tickSend = setInterval(() => {
            this.props.sendWSData(this.state.codes || '');
        }, 20000);
    };

    stopData = () => {
        this.props.stopWSData();
    };

    render() {
        return (
            <InputWrapper>
                <Head>
                    <img src={iconInput} alt=""/>
                    <span>Input</span>
                </Head>
                <Body>
                    <Label>
                        <span>Type question, text or pic</span>
                        <button onClick={this.sendData}>SEND</button>
                        <button onClick={this.stopData}>STOP</button>
                    </Label>
                    <Input value={this.state.codes} onChange={this.changeCodes}/>
                </Body>
                <img src={gradient} alt=""/>
            </InputWrapper>
        );
    }
}

export default InputBox;
