import React from 'react';
import styled from 'styled-components';
import InputBox from './InputBox';
import OutputBox from './OutputBox';

const TerminalWrapper = styled.div`
    grid-area: terminal;
    display: flex;
    flex-direction: column;
`;

class Terminal extends React.Component {

    constructor(props) {
        super(props);

        const wsHost = 'wss://oneai.network/net';
        this.websocket = new WebSocket(wsHost);
        this.ping = null;

        this.ping = setInterval(() => {
            this.websocket.send('ping');
        }, 50000);

        this.websocket.onopen = (evt) => { this.onOpen(evt); };
        this.websocket.onclose = (evt) => { this.onClose(evt); };
        this.websocket.onmessage = (evt) => { this.onMessage(evt); };
        this.websocket.onerror = (evt) => { this.onError(evt); };
    }

    onOpen = (evt) => {
        console.log('[onOpen]', 'CONNECTED');
    };

    onClose = (evt) => {
        clearInterval(this.ping);
        console.log('[onClose]', 'DISCONNECTED');
    };

    onMessage = (evt) => {
        if(evt.data !== 'pong') {
            // console.log('[onMessage]', evt.data);
            this.props.setWsMsg(evt.data);
            this.forceUpdate();
        }
    };

    onError = (evt) => {
        console.log('[onError]', evt.data);
    };

    sendWSData = (codes) => {
        if(this.websocket.readyState === this.websocket.OPEN) {
            this.props.resetWsMsg();
            const txt = JSON.stringify({ cmd: 'run_code', code: codes });
            this.websocket.send(txt);
        } else {
            console.log('websocket is not connected');
        }
    };

    stopWSData = () => {
        this.websocket.close();
    };

    render() {
        const { wsMsg } = this.props;
        return (
            <TerminalWrapper>
                <InputBox
                    sendWSData={this.sendWSData}
                    stopWSData={this.stopWSData}
                />
                <OutputBox wsMsg={wsMsg}/>
            </TerminalWrapper>
        );
    }
}

export default Terminal;
