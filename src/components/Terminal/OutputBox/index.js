import React from 'react';
import styled from 'styled-components';

import LvMenu from './LvMenu';
import iconOutput from '../assets/output.svg';

const OutputWrapper = styled.div`
    width: 100%;
    height: 740px;
    background: ${props => props.theme.palette.clrContrastText};
    flex: 1;
    border-radius: 8px;
    display: flex;
    flex-direction: column;
    overflow: scroll;
`;

const Head = styled.div`
    padding: 27px 64px 27px 22px;
    display: flex;
    align-items: center;
    
    > span {
        padding-left: 22px;
        font-size: 16px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.5;
        letter-spacing: normal;
        text-align: left;
        color: ${props => props.theme.palette.clrText};
        flex: 1;
    }
    
    @media(max-width: 1500px){
        padding-right: 22px;
    }
`;

const Content = styled.div`
    padding: 10px 30px;
`;

class OutputBox extends React.Component {
    render() {
        const { wsMsg } = this.props;
        let message = [];
        for (let i = 0; i < wsMsg.length; i++) {
            message.push(<div key={i + 'out'}>{wsMsg[i]}</div>);
            message.push(<br key={i + 'break'}/>);
        }
        return (
            <OutputWrapper>
                <Head>
                    <img src={iconOutput} alt=""/>
                    <span>Output</span>
                    <LvMenu/>
                </Head>
                <Content>
                    { message }
                </Content>
            </OutputWrapper>
        );
    }
}

export default OutputBox;
