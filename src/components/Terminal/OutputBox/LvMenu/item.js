import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    font-size: 16px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    text-align: left;
    color: ${props => props.isActive ? props.theme.palette.clrHover : props.theme.palette.clrText};
    flex: 1;
    text-align: center;
    padding-bottom: 12px;
    border-bottom: 4px solid ${props => props.isActive ? props.theme.palette.clrHover : props.theme.palette.clrBackground};
    cursor: pointer;
    max-width: 70px;
    padding-left: 12px;
    padding-right: 12px;
    white-space: nowrap;
    
    @media(max-width: 1424px) and (min-width: 1024px) {
        font-size: 13px;
        line-height: 1;
    }
    
    @media(max-width: 768px) {
        padding-left: 5px;
        padding-right: 5px;
        font-size: 13px;
        line-height: 1;
    }
`;

class LvMenuItem extends React.Component {
    render() {
        const { level, isActive, setIndex } = this.props;

        return (
            <Wrapper
                isActive={isActive}
                onClick={() => {
                    setIndex(level - 1);
                }}
            >
                Level {level}
            </Wrapper>
        );
    }
}

export default LvMenuItem;
