import React from 'react';
import styled from 'styled-components';

import LvMenuItem from './item';

const Wrapper = styled.div`
    display: flex;
    align-items: center;
`;

class LvMenu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            index: 0,
        };
    }

    setIndex = (idx) => {
        this.setState({
            index: idx,
        });
    }

    render() {
        const { index } = this.state;
        let menuItems = [];
        for (let i = 0; i < 7; i++) {
            menuItems.push(
                <LvMenuItem key={i} level={i + 1} isActive={index === i} setIndex={this.setIndex}/>
            );
        }

        return (
            <Wrapper>
                { menuItems }
            </Wrapper>
        );
    }
}

export default LvMenu;
