import once from 'lodash.once';
import SessionStore from './SessionStore';

const SESSIONSTORE = 'SessionStore';

export const STORE_KEYS = {
    SESSIONSTORE,
};

export default once(() => {
    return ({
        [STORE_KEYS.SESSIONSTORE]: SessionStore(),
    });
});
