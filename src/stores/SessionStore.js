import { observable } from 'mobx';

class SessionStore {
    @observable price = 0;
}
export default () => new SessionStore();
