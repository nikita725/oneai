const createKeysEnum = keys => keys.reduce(
    (acc, key) => {
        acc[key] = key;
        return acc;
    }, {}
);

// Keys used in both dark & light theme
const rootKeys = createKeysEnum([
    'palette',
    'muiTheme'
]);

const corePalette = {
    clrBackground: '#ecebed',
    clrContrastText: '#fff',
    clrText: '#a2a1b3',
    clrHover: '#f2556f',
    clrDarkText: '#46454d',
    clrGreen: '#41d98d',
    clrBlue: '#4595e6',

    regularSpacer: '15px',
    cornerRadius: '3px',
    defaultStrokeWeight: '1px',
    portfolioChartStrokeWeight: '3px',
};

// themes
export const mainTheme = {
    [rootKeys.muiTheme]: 'dark',

    [rootKeys.palette]: {
        ...corePalette,
    },
};
