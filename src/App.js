import React from 'react';
import { Provider } from 'mobx-react';
import { ThemeProvider } from 'styled-components';

import Stores from './stores';
import MainGrid from './grid/MainGrid';
import { mainTheme } from './theme/core';

const App = () => (
    <Provider {...Stores()}>
        <ThemeProvider theme={mainTheme}>
            <MainGrid/>
        </ThemeProvider>
    </Provider>
);

export default App;
